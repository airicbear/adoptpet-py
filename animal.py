#!/usr/bin/env python3

class Animal:

  # This is called a constructor
  def __init__(self, type="AnimalType", name="AnimalName", age=0, value=0):
    self.type = type
    self.name = name
    self.age = age
    self.value = value

  # This defines what the string version of this class will return
  def __str__(self):
    return "Type: " + self.type + "\nName: " + self.name + "\nAge: " + str(self.age) + "\nValue: $" + str(self.value) + "\nID: " + str(id(self))

# This is what happens when you run "python animal.py"
if __name__ == "__main__":
  print(Animal())