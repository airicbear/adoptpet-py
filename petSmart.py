#!/usr/bin/env python3

import get
import animal
import random

petList = [
  animal.Animal(type="GOAT", value=random.randint(75, 300)), 
  animal.Animal(type="DOG", value=random.randint(0, 350)), 
  animal.Animal(type="CAT", value=random.randint(50, 150)), 
  animal.Animal(type="HAMSTER", value=random.randint(5, 20))
]

def getType(pet=animal.Animal(type="Goat")):
  pet.type = input("Which pet would you like to adopt? ")

  if pet.type == "":
    return getType(pet)
    
  if pet.type.upper() not in [animal.type for animal in petList]:
    print("We don't currently have any " + pet.type.lower() + "s available for adoption.")
    return getType(pet)

  return pet.type

def getName(pet=animal.Animal(name="Doge")):
  return input("What's your " + pet.type.lower() + "'s name? ")

def getAge(pet=animal.Animal(age=0)):
  age = get.integer("How old is " + pet.name + "? ")
  if age < 1:
    print("You must enter a positive number.")
    return getAge(pet)
  return age

def getCount(min=0, max=100):
  petCount = get.integer("How many pets you like to adopt? ")
  if petCount > min and petCount <= max: 
    return petCount
  elif petCount > max:
    print("You are not allowed to adopt that many pets at once. Sorry.")
  elif petCount <= min:
    print("You must adopt more than", min, "pets.")

def printPetList():
  print("PetSmart's Pet List: " + ", ".join([pet.type for pet in petList]))

def adoptPet():
  printPetList()
  petCount = getCount()
  userPets = [0] * petCount
  for i in range(len(userPets)):
    pet = animal.Animal()
    pet.type = getType(pet)
    pet.name = getName(pet)
    pet.age = getAge(pet)
    pet.value = petList[[pet.type.upper() for pet in petList].index(pet.type.upper())].value
    userPets[i] = pet

  sep = lambda len: "\n" + "=".join(["" for _ in range(len)]) + "\n"
  wrapSep = lambda string, len: sep(len) + string + sep(len)
  print(wrapSep("YOUR NEW PETS!", len=14) + "\n".join([wrapSep(string=pet.name.upper(), len=(len(pet.name) + 1)) + str(pet) for pet in userPets]))